package josergc;

import java.io.IOException;
import java.io.Reader;

public class CharArrayReader extends Reader {
	
	private int previousIndex;
	private int i;
	private char[] ca;
	private int previousTopLimit = -1;
	private int topLimit;

	public CharArrayReader(String s) {
		this(s.toCharArray(),0,s.length());
	}

	public CharArrayReader(char[] caIn, int i, int length) {
		System.arraycopy(caIn,i,ca = new char[length - i],0,topLimit = length);
	}

	public void close() throws IOException {
	}

	public int read(char[] caOut,int offset,int length) throws IOException {
		if (topLimit - i < length) {
			int diff = length - topLimit + i;
			System.arraycopy(ca,i,caOut,offset,diff);
			i += diff;
			return diff;
		}
		System.arraycopy(ca,i,caOut,offset,length);
		i += length;
		return length;
	}
	
	public boolean markSupported() {
		return true;
	}
	
	public void mark(int readAheadLimit) throws IOException {
		if (i + readAheadLimit >= topLimit) {
			throw new IOException();
		}
		previousIndex = i;
		previousTopLimit = topLimit;
		topLimit = i + readAheadLimit;
	}
	
	public void reset() throws IOException {
		if (previousTopLimit == -1) {
			throw new IOException();
		}
		i = previousIndex;
		topLimit = previousTopLimit;
		previousTopLimit = -1;
	}

}
